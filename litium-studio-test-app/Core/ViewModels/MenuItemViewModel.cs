﻿using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Products;
using Litium;
using litium_studio_test_app.Interfaces;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Studio.Builders;

namespace litium_studio_test_app.Core.ViewModels
{
    public class MenuItemViewModel
    {
        private readonly CategoryService _categoryService;
        private readonly IProductViewModelBuilder _productModelBuilder;

        public MenuItemViewModel()
        {
            Children = new List<MenuItemViewModel>();
        }
        public MenuItemViewModel(Page page)
        {
            _productModelBuilder = IoC.Resolve<IProductViewModelBuilder>();
            _categoryService = IoC.Resolve<CategoryService>();

            if (page == null)
                return;

          



            var pageChildren = Page.GetFromIDs(page.Children.ChildrenIDs, SecurityToken.CurrentSecurityToken);

            foreach (var child in pageChildren)
            {
                if (child.Status == PageStatus.PUBLISHED)
                {
                    if (child.Status == PageStatus.PUBLISHED)
                        Children.Add(new MenuItemViewModel(child));
                }

            }
        }
        public MenuItemViewModel(Category category, bool mobileonly)
        {

            Url = category.GetUrl(CurrentState.Current.WebSiteID);
            Name = category.Localizations.CurrentCulture.SeoTitle ?? category.Localizations.CurrentCulture.Name;
            MobileOnly = mobileonly;
        }
        public MenuItemViewModel(string name, List<MenuItemViewModel> menuViewModel)
        {
            Name = name;
            Children = menuViewModel;
        }

     

        public string Name { get; set; }
        public string Url { get; set; }
        public List<MenuItemViewModel> Children { get; set; }
        public bool MobileOnly { get; set; }
        public bool IsSelected { get; set; }
        public string HtmlContent { get; set; }
    }
}