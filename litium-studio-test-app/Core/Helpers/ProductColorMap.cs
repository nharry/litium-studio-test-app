﻿using Litium;
using Litium.Owin.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;

namespace litium_studio_test_app.Core.Helpers
{
    public static class ProductColorMap
    {
        public static string FileName = "productcolormap.xml";
        public static string FilePath = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, FileName);
        private static readonly ILog Logger = MethodBase.GetCurrentMethod().DeclaringType.Log();
        private static System.Web.Caching.CacheDependency _cacheDependency;
        private static Dictionary<string, string> _colorMap;

        static ProductColorMap()
        {
            CreateMap();
        }

        public static string GetColorCodeByName(string colorName)
        {
            if (colorName != null)
            {
                if (_colorMap == null || _cacheDependency.HasChanged)
                {
                    CreateMap();
                }

                if (_colorMap != null)
                {
                    string colorCode;

                    if (_colorMap.TryGetValue(colorName.ToUpperInvariant(), out colorCode))
                    {
                        return colorCode;
                    }
                }
            }

            return null;
        }

        private static void CreateMap()
        {
            try
            {
                var xDoc = XDocument.Load(FilePath);
                _colorMap = xDoc.Root.Elements()
                    .ToDictionary(
                        x => (string)x.Elements("name").First(),
                        x => (string)x.Elements("code").First()
                    );
                _cacheDependency = new System.Web.Caching.CacheDependency(FilePath);
            }
            catch (Exception ex)
            {
                Logger.Error("An error occured when trying to create the product color map.", ex);
            }
        }
    }
}