﻿using litium_studio_test_app.Core.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using litium_studio_test_app.Core.ViewModels;
using Litium.Foundation.Modules.CMS;
using Litium.Products;

namespace litium_studio_test_app.Core.Services
{
    public class AssortmentService : IAssortmentService
    {
        private readonly CategoryService _categoryService;

        public AssortmentService(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public List<MenuItemViewModel> GetAssortmentMenuLinks(Category currentCategory)
        {
            IEnumerable<Category> parent = _categoryService.GetChildCategories(Guid.Empty, CurrentState.Current.WebSite.AssortmentId);

            List<MenuItemViewModel> menuItemViewModels = new List<MenuItemViewModel>();
            foreach (var child in parent)
            {
                MenuItemViewModel menuItemViewModel = new MenuItemViewModel(child, false);
                if (currentCategory != null && child == currentCategory)
                {
                    menuItemViewModel.IsSelected = true;
                }
                else
                {
                    menuItemViewModel.IsSelected = false;
                }
                menuItemViewModels.Add(menuItemViewModel);
            }
            return menuItemViewModels;
        }
    }
}