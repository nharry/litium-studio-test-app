﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.ExtensionMethods;

namespace litium_studio_test_app.Core.Services
{
    public class ImageViewModelBuilder : IImageViewModelBuilder
    {
        public ImageViewModel CreateImageViewModel(Guid guid)
        {
            var maxSize = new Size(1000, 1000); // TODO: Generate predefined image size, S/M/L etc
            var minSize = new Size(100, 100);
            Size size;
            string fileName;
            string contentType;

            var url = guid.GetImageUrl(maxSize, minSize, out size, out fileName, out contentType);

            var imageViewModel = new ImageViewModel
            {
                Url = url,
                FileName = fileName
            };
            return imageViewModel;
        }
    }
}