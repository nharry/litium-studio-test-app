﻿using Litium.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using litium_studio_test_app.Interfaces;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Studio.Builders.CMS;
using Litium.Web.Models.Products;
using Litium.Foundation.Modules.CMS.Content;

namespace litium_studio_test_app.Core.Services
{
    public class ProductService : IProductService
    {
        private readonly RelationshipTypeService _relationshipTypeService;
        private readonly ProductModelBuilder _productModelBuilder;
        private readonly RelatedModelBuilder _relatedModelBuilder;
        private readonly ModuleECommerce _moduleECommerce;
        private readonly BaseProductService _baseProductService;
        private readonly VariantService _variantService;

        public ProductService(RelationshipTypeService relationshipTypeService,
            ProductModelBuilder productModelBuilder, RelatedModelBuilder relatedModelBuilder,
            ModuleECommerce moduleECommerce, BaseProductService baseProductService, VariantService variantService)
        {
            _relationshipTypeService = relationshipTypeService;
            _productModelBuilder = productModelBuilder;
            _relatedModelBuilder = relatedModelBuilder;
            _moduleECommerce = moduleECommerce;
            _baseProductService = baseProductService;
            _variantService = variantService;
        }

        public RelatedModel GetVariantRelationships(string variantId, string relationTypeName,
            bool includeBaseProductRelations = true, bool includeVariantRelations = true)
        {
            if (string.IsNullOrEmpty(relationTypeName))
            {
                return null;
            }
            var productModel = _productModelBuilder.BuildFromVariant(_variantService.Get(variantId));
            return GetBaseProductRelationshipsHelper(relationTypeName, includeBaseProductRelations,
                includeVariantRelations, productModel);
        }

        public RelatedModel GetBaseProductRelationships(BaseProduct baseProduct, string relationTypeName,
            bool includeBaseProductRelations = true, bool includeVariantRelations = true)
        {
            if (string.IsNullOrEmpty(relationTypeName))
            {
                return null;
            }
            var productModel = _productModelBuilder.BuildFromBaseProduct(baseProduct, CurrentState.Current.WebSite);
            return GetBaseProductRelationshipsHelper(relationTypeName, includeBaseProductRelations,
                includeVariantRelations, productModel);
        }

        private RelatedModel GetBaseProductRelationshipsHelper(string relationTypeName, bool includeBaseProductRelations,
            bool includeVariantRelations, ProductModel productModel)
        {
            if (productModel == null)
            {
                return null;
            }
            var relationshipType = _relationshipTypeService.Get(relationTypeName);
            if (relationshipType == null)
            {
                return null;
            }
            var model = _relatedModelBuilder.Build(CurrentState.Current.WebSite,
                productModel,
                relationshipType,
                includeBaseProductRelations,
                includeVariantRelations);

            return model;
        }


        public List<Variant> FilterProductList(PageTypeDefinition currentPageTypeDefinition,
            PropertyCollection currentSettings, Guid productList, DateTime timeStamp, bool randomProductOrder,
            int top = -1)
        {
            var otherOffers = new List<Variant>();
            var products = productList.GetProductList();
            if (products != null)
            {
                otherOffers = FilterDataSource(products,
                    randomProductOrder, top);
            }

            return otherOffers;
        }

        /// <summary>
        /// Filter out what products to include in datasource.
        /// </summary>
        /// <param name="productList">The product set.</param>
        /// <param name="randomProductOrder"></param>
        /// <param name="top"></param>
        /// <returns>
        /// Filtered list.
        /// </returns>
        private List<Variant> FilterDataSource(ProductList productList, bool randomProductOrder,
            int top)
        {
            if (productList == null)
                return new List<Variant>();

            var result = productList.GetPublishedActiveVariants(CurrentState.Current.WebSite.ID).ToList();
            var filteredProducts = new List<Variant>();

            var numberOfProductToDisplay = 10;

            // Get random products
            if (randomProductOrder && numberOfProductToDisplay > 0)
            {
                var random = new Random();
                var randomProducts = new Dictionary<string, Variant>();

                // Iterate while  
                while (randomProducts.Count < numberOfProductToDisplay && result.Count > 0)
                {
                    var position = random.Next(result.Count);

                    var product = result[position];
                    var selectedVariantSystemId = product.Id;
                    if (!randomProducts.ContainsKey(selectedVariantSystemId))
                    {
                        randomProducts.Add(selectedVariantSystemId, product);
                    }

                    result.Remove(product);
                }

                filteredProducts.AddRange(randomProducts.Values.ToList());
            }
            else
            {
                filteredProducts.AddRange(result.Take(numberOfProductToDisplay));
            }

            return filteredProducts;
        }
    }
}