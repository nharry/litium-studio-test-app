﻿//using Litium.FieldFramework;
//using Litium.Foundation.Modules.CMS.Pages;
//using Litium.Foundation.Modules.ExtensionMethods;
//using Litium.Products;
//using litium_studio_test_app.Interfaces;
//using litium_studio_test_app.ViewModels;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Web;
//using litium_studio_test_app.Core.Services.Contracts;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using litium_studio_test_app.Core.Services.Contracts;
using Litium.FieldFramework;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Products;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;

namespace litium_studio_test_app.Core.Services
{
    #region 
    //public class CategoryPageViewModelBuilder : ICategoryPageViewModelBuilder
    //{
    //    private readonly BaseProductService _baseProductService;
    //    private readonly IImageViewModelBuilder _imageViewModelBuilder;
    //    private readonly IProductViewModelBuilder _productViewModelBuilder;
    //    private readonly IAssortmentService _assortmentService;

    //    public CategoryPageViewModelBuilder(IImageViewModelBuilder imageViewModelBuilder, IProductViewModelBuilder productViewModelBuilder, BaseProductService baseProductService, IAssortmentService assortmentService)
    //    {
    //        _imageViewModelBuilder = imageViewModelBuilder;
    //        _productViewModelBuilder = productViewModelBuilder;
    //        _baseProductService = baseProductService;
    //        _assortmentService = assortmentService;
    //    }

    //    public CategoryPageViewModel CreateCategoryPageViewModel(Category currentCategory, Page page, Guid WebSiteId)
    //    {

    //        var localization = currentCategory.Localizations.CurrentCulture;

    //        var products = GetProducts(true, currentCategory, WebSiteId);
    //        var images = GetImages(currentCategory);

    //        var categoryViewModel = new CategoryViewModel
    //        {
    //            Title = localization.SeoTitle ?? localization.Name,
    //            Description = localization.Description,
    //            Images = images
    //        };

    //        var categoryPageViewModel = new CategoryPageViewModel
    //        {
    //            CategoryViewModel = categoryViewModel,
    //            ProductList = products,
    //            MenuLinkList = _assortmentService.GetAssortmentMenuLinks(currentCategory)
    //        };
    //        return categoryPageViewModel;
    //    }

    //    private ReadOnlyCollection<ImageViewModel> GetImages(Category category)
    //    {
    //        var imageIDs = category.Fields.GetValue<IList<Guid>>(SystemFieldDefinitionConstants.Images);

    //        if (imageIDs == null)
    //            return new ReadOnlyCollection<ImageViewModel>(new List<ImageViewModel>());

    //        var images = new ReadOnlyCollection<ImageViewModel>(imageIDs.Select(id => _imageViewModelBuilder.CreateImageViewModel(id)).ToList());

    //        return images;
    //    }
    //    public List<ProductViewModel> GetProducts(bool includeVariants, Category category, Guid WebSiteId)
    //    {
    //        var products = new List<ProductViewModel>();
    //        foreach (var prodID in category.ProductLinks)
    //        {
    //            BaseProduct baseProduct = _baseProductService.Get(prodID.BaseProductSystemId);
    //            if (baseProduct.IsPublished(WebSiteId))
    //            {

    //                ProductViewModel prod = _productViewModelBuilder.CreateProductViewModelFromBaseProduct(baseProduct, DateTime.Now, WebSiteId);

    //                if (prod == null)
    //                {
    //                    continue;
    //                }
    //                if (includeVariants)
    //                {
    //                    foreach (var variant in prod.Variants.Select((value, i) => new { i, value }))
    //                    {
    //                        if (variant.value == null)
    //                        {
    //                            continue;
    //                        }
    //                        ProductViewModel productCopy = prod.Copy();
    //                        productCopy.SelectedVariantIndex = variant.i;
    //                        products.Add(productCopy);
    //                    }
    //                }
    //                else
    //                {
    //                    prod.SelectedVariantIndex = 0;
    //                    products.Add(prod);
    //                }
    //            }
    //        }
    //        return products;
    //    }
    //}
    #endregion


    public class CategoryPageViewModelBuilder : ICategoryPageViewModelBuilder
    {
        private readonly BaseProductService _baseProductService;
        private readonly IImageViewModelBuilder _imageViewModelBuilder;
        private readonly IProductViewModelBuilder _productViewModelBuilder;
        private readonly IAssortmentService _assortmentService;


        public CategoryPageViewModelBuilder(IImageViewModelBuilder imageViewModelBuilder, IProductViewModelBuilder productViewModelBuilder, BaseProductService baseProductService, IAssortmentService assortmentService)
        {
            _imageViewModelBuilder = imageViewModelBuilder;
            _productViewModelBuilder = productViewModelBuilder;
            _baseProductService = baseProductService;
            _assortmentService = assortmentService;
        }

        public CategoryPageViewModel CreateCategoryPageViewModel(Category currentCategory, Page page, Guid WebSiteId)
        {

            var localization = currentCategory.Localizations.CurrentCulture;

            var products = GetProducts(true, currentCategory, WebSiteId);
            var images = GetImages(currentCategory);

            var categoryViewModel = new CategoryViewModel
            {
                Title = localization.SeoTitle ?? localization.Name,
                Description = localization.Description,
                Images = images
            };

            var categoryPageViewModel = new CategoryPageViewModel
            {
                CategoryViewModel = categoryViewModel,
                ProductList = products,
                MenuLinkList = _assortmentService.GetAssortmentMenuLinks(currentCategory)
            };
            return categoryPageViewModel;
        }

        private ReadOnlyCollection<ImageViewModel> GetImages(Category category)
        {
            var imageIDs = category.Fields.GetValue<IList<Guid>>(SystemFieldDefinitionConstants.Images);

            if (imageIDs == null)
                return new ReadOnlyCollection<ImageViewModel>(new List<ImageViewModel>());

            var images = new ReadOnlyCollection<ImageViewModel>(imageIDs.Select(id => _imageViewModelBuilder.CreateImageViewModel(id)).ToList());

            return images;
        }
        public List<ProductViewModel> GetProducts(bool includeVariants, Category category, Guid WebSiteId)
        {
            var products = new List<ProductViewModel>();
            foreach (var prodID in category.ProductLinks)
            {
                BaseProduct baseProduct = _baseProductService.Get(prodID.BaseProductSystemId);
                if (baseProduct.IsPublished(WebSiteId))
                {

                    ProductViewModel prod = _productViewModelBuilder.CreateProductViewModelFromBaseProduct(baseProduct, DateTime.Now, WebSiteId);

                    if (prod == null)
                    {
                        continue;
                    }
                    if (includeVariants)
                    {
                        foreach (var variant in prod.Variants.Select((value, i) => new { i, value }))
                        {
                            if (variant.value == null)
                            {
                                continue;
                            }
                            ProductViewModel productCopy = prod.Copy();
                            productCopy.SelectedVariantIndex = variant.i;
                            productCopy.Id = variant.value.Id;
                            productCopy.Name = variant.value.Name;
                            productCopy.Description = variant.value.Description;
                            productCopy.Images = variant.value.Images;
                            productCopy.ProductBaseColor = variant.value.VariantBaseColor;
                           
                            products.Add(productCopy);
                        }
                    }
                    else
                    {
                        prod.SelectedVariantIndex = 0;
                        products.Add(prod);
                    }
                }
            }
            return products;
        }
    }





}