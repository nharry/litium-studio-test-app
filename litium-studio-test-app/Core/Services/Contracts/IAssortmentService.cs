﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Litium.Products;
using litium_studio_test_app.Core.ViewModels;

namespace litium_studio_test_app.Core.Services.Contracts
{
    public interface IAssortmentService
    {
        List<MenuItemViewModel> GetAssortmentMenuLinks(Category category);
    }
}
