﻿using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce.Carriers;
using Litium.Products;
using litium_studio_test_app.Core.Services.Contracts;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace litium_studio_test_app.Core.Services
{
    public class CartService : ICartService
    {
        private readonly ICartItemViewModelBuilder _cartItemViewModelBuilder;
        private readonly IProductViewModelBuilder _productViewModelBuilder;

        public CartService(ICartItemViewModelBuilder cartItemViewModelBuilder, IProductViewModelBuilder productViewModelBuilder)
        {
            _cartItemViewModelBuilder = cartItemViewModelBuilder;
            _productViewModelBuilder = productViewModelBuilder;
        }

        public CartViewModel GetCart()
        {
            var cart = new CartViewModel();
            Decimal total = 0;
            List<PriceViewModel> PriceView = new List<PriceViewModel>();          
            cart.Items = CurrentState.Current.ShoppingCart.OrderCarrier.OrderRows.Select(x => _cartItemViewModelBuilder.CreateCartItemViewModel(x)).ToList();

            #region Newely Added

            var variantService = IoC.Resolve<VariantService>();

            foreach (var item in cart.Items)
            {
                var selectedItem = variantService.Get(item.ProductViewModel.Id);
                total = total + selectedItem.Prices.Select(x => x.Price).FirstOrDefault();
            }

            cart.TotalProductPrice = total;

            #endregion


            //AddDummyProducts();
            //PriceView = CurrentState.Current.ShoppingCart.OrderCarrier.OrderRows.Select(x => _cartItemViewModelBuilder.CreateVariantViewModel(x).Price).ToList();
            //cart.TotalProductPrice = getTotalPriceInclVat(PriceView, cart.Items);
            //cart.TotalPrice = getTotalPrice(PriceView, cart.Items);
            //cart.TotalSaved = cart.TotalProductPrice - cart.TotalPrice;
            //cart.CommonVat = getCommonVat(PriceView, cart.Items);
            //cart.TotalDeliveryCost = (int)CurrentState.Current.ShoppingCart.OrderCarrier.TotalDeliveryCostWithVAT;

            //if (System.Web.HttpContext.Current.Request.Cookies["shoppingCartItems"] != null)
            //{
            //    string cookieData = System.Web.HttpContext.Current.Request.Cookies["shoppingCartItems"].Value;
            //    if (cookieData != null)
            //    {
            //        string[] dataArray = cookieData.Split(',');
            //        foreach (var items in dataArray)
            //        {
            //            if (items != "")
            //            {
            //                var data = items;
            //                string[] dataList = data.Split(':');
            //                var articleNo = dataList[0];
            //                var quantity = dataList[1];
            //                if (cart.Items.Count == 0)
            //                {
            //                    AddItem(articleNo, Int32.Parse(quantity));
            //                }
            //            }
            //        }
            //    }
            //}

            return cart;
        }

        public void AddItem(string articleNumber, int quantity)
        {
           
            CurrentState.Current.ShoppingCart.Add(articleNumber, quantity, string.Empty, CurrentState.Current.WebSite.Language.ID);
         
        }

        public void RemoveItem(string articleNumber)
        {
            CurrentState.Current.ShoppingCart.RemoveProduct(articleNumber);
        }

        public void UpdateItem(string articleNumber, int newQuantity)
        {
            var cart = CurrentState.Current.ShoppingCart;
            OrderRowCarrier row = cart.OrderCarrier.OrderRows.Find(x => x.ArticleNumber.Equals(articleNumber) && !x.CarrierState.IsMarkedForDeleting);
            if (row != null)
            {
                if (newQuantity != row.Quantity && newQuantity >= 0)
                {
                    cart.UpdateRowQuantity(row.ID, newQuantity);
                    cart.UpdateChangedRows();
                }
            }
        }
        private void AddDummyProducts()
        {
            if (CurrentState.Current.ShoppingCart.OrderCarrier == null || CurrentState.Current.ShoppingCart.OrderCarrier.OrderRows.Count == 0)
            {
                AddItem("74208_35_00000", 1);
                AddItem("74208_06_00000", 2);
            }
        }


        private decimal getTotalPriceInclVat(List<PriceViewModel> PriceView, List<CartItemViewModel> Items)
        {
            decimal total = 0.0M;
            int count = Items.Count();
            //for (int item = 0; item < Items.Count; item++) total = total + (PriceView[item].BasePrice.PriceInclVat * Items[item].Quantity);

            for (int item = 0; item < Items.Count; item++)
            {
                total = total + Items[item].ProductViewModel.ProductPrice;
            }

            return Math.Round(total, 2);
        }


        private decimal getTotalPrice(List<PriceViewModel> Price, List<CartItemViewModel> Items)
        {
            decimal total = 0.0M, min = 0.0M;
            for (int item = 0; item < Items.Count; item++)
            {
                min = Price[item].BasePrice.PriceInclVat;
                if (Price[item].DiscountPrice != null && min > Price[item].DiscountPrice.PriceInclVat)
                    min = Price[item].DiscountPrice.PriceInclVat;

                if (Price[item].ClubPrice != null && min > Price[item].ClubPrice.PriceInclVat)
                    min = Price[item].ClubPrice.PriceInclVat;

                total += min * Items[item].Quantity;
            }
            return Math.Round(total, 2);
        }


        private decimal getCommonVat(List<PriceViewModel> Price, List<CartItemViewModel> Items)
        {
            decimal total = 0.0M, combinedPrice = 0.0M, priceInclVat = 0.0M, priceExclVat = 0.0M;
            for (int item = 0; item < Items.Count; item++)
            {
                if (Price[item].DiscountPrice != null)
                {
                    priceInclVat = Price[item].DiscountPrice.PriceInclVat;
                    priceExclVat = Price[item].DiscountPrice.Price;
                    combinedPrice = priceInclVat - priceExclVat;
                }
                else
                {
                    priceInclVat = Price[item].BasePrice.PriceInclVat;
                    priceExclVat = Price[item].BasePrice.Price;
                    combinedPrice = priceInclVat - priceExclVat;
                }

                total += Items[item].Quantity * combinedPrice;
            }
            return Math.Round(total, 2);
        }

       
    }
}