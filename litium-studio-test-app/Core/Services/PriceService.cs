﻿using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ECommerce.Plugins.PriceCalculator;
using Litium.Foundation.Security;
using Litium.Products;
using Litium.Products.PriceCalculator;
using Litium.Web.Models.Products;
using litium_studio_test_app.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace litium_studio_test_app.Core.Services
{
    public class PriceService : IPriceService
    {
        private readonly IPriceCalculator _priceCalculator;
        private readonly ICampaignPriceCalculator _campaignPriceCalculator;
        private readonly PriceListService _priceListService;
        private readonly WebSiteStrings _webSiteStrings;

        private const string BasePriceList = "XXA01";
        private const string ClubPriceList = "KLUBB";

        private List<string> _ignoreLists = new List<string> { "GRUND" };



        public PriceService(IPriceCalculator priceCalculator, ICampaignPriceCalculator campaignPriceCalculator, PriceListService priceListService, WebSiteStrings webSiteStrings)
        {
            _priceCalculator = priceCalculator;
            _campaignPriceCalculator = campaignPriceCalculator;
            _priceListService = priceListService;
            _webSiteStrings = webSiteStrings;
        }

        public IEnumerable<PriceModel> GetPriceModels(ProductModel productModel, DateTime currentTime)
        {
            var priceCalculatorArgs = new PriceCalculatorArgs
            {
                CurrencySystemId = CurrentState.Current.ShoppingCart.Currency.SystemId,
                DateTimeUtc = currentTime.ToUniversalTime(),
                UserSystemId = SecurityToken.CurrentSecurityToken.UserID,
                WebSiteSystemId = CurrentState.Current.WebSite.ID
            };

            var availibleLists = _priceCalculator.GetPriceLists(priceCalculatorArgs);

            var prices = productModel.SelectedVariant.Prices.Where(p => availibleLists.Any(al => al.SystemId == p.PriceListSystemId));
            var pricedata =
                prices.Select(
                        p => new
                        {
                            p.MinimumQuantity,
                            p.Price,
                            p.VatPercentage,
                            PriceList = _priceListService.Get(p.PriceListSystemId),
                            
                        })
                    .Where(pd => pd.PriceList.WebSiteLinks.Any(wsl => wsl.WebSiteSystemId == CurrentState.Current.WebSiteID) && !_ignoreLists.Contains(pd.PriceList.Id.Remove(0, 4)))
                    .Select(pd => new PriceModel { MinimumQuantity = pd.MinimumQuantity, Price = pd.Price, VatPercentage = pd.VatPercentage, Type = GetPriceType(pd.PriceList.Id.Remove(0, 4)) });

            return pricedata;

            //var priceCalculatorItemArgs = new PriceCalculatorItemArgs
            //{
            //    VariantSystemId = productModel.SelectedVariant.SystemId,
            //    Quantity = 1
            //};



            //PriceCalculatorResult calculatorResult;
            //var listPrices = _priceCalculator.GetListPrices(priceCalculatorArgs, priceCalculatorItemArgs);
            //listPrices.TryGetValue(priceCalculatorItemArgs.VariantSystemId, out calculatorResult);

            //return calculatorResult;
        }


        private PriceType GetPriceType(string priceListId)
        {
            switch (priceListId)
            {
                case BasePriceList:
                    return PriceType.Base;
                case ClubPriceList:
                    return PriceType.Club;
                default:
                    return PriceType.Discount;
            }
        }

        public CampaignPriceResult GetCampaignPrice(ProductModel productModel, DateTime currentTime)
        {
            var priceCalculatorArgs = new CampaignPriceCalculatorArgs()
            {
                VariantSystemId = productModel.SelectedVariant.SystemId,
                CurrencySystemId = CurrentState.Current.ShoppingCart.Currency.SystemId,
                DateTimeUtc = currentTime.ToUniversalTime(),
                LanguageID = CurrentState.Current.WebSite.Language.ID,
                Quantity = decimal.One,
                SecurityToken = SecurityToken.CurrentSecurityToken,
                SourceId = null,
                UserID = SecurityToken.CurrentSecurityToken.UserID,
                WebSiteID = CurrentState.Current.WebSite.ID
            };

            CampaignPriceResult campaignPriceResult;
            if (!_campaignPriceCalculator.GetCampaignPrices(priceCalculatorArgs).TryGetValue(priceCalculatorArgs.VariantSystemId, out campaignPriceResult))
            {
                return null;
            }

            return campaignPriceResult;
        }

        public bool IsPriceFound(ProductModel currentVariant, DateTime timeStamp)
        {
            try
            {
                var websiteCurrency = CurrentState.Current.WebSite.Currency;
                if (currentVariant == null)
                {
                    return false;
                }

                // get price from price calculator.
                var securityToken = CurrentState.Current.Token;
                var priceCalculatorArgs = new PriceCalculatorArgs
                {
                    CurrencySystemId = websiteCurrency.ID,
                    DateTimeUtc = timeStamp.ToUniversalTime(),
                    UserSystemId = securityToken.UserID,
                    WebSiteSystemId = CurrentState.Current.WebSite.ID
                };

                var priceCalculatorItemArgs = new PriceCalculatorItemArgs
                {
                    VariantSystemId = currentVariant.SelectedVariant.SystemId,
                    Quantity = 1,
                };

                PriceCalculatorResult listPrice;
                return _priceCalculator.GetListPrices(priceCalculatorArgs, priceCalculatorItemArgs).TryGetValue(priceCalculatorItemArgs.VariantSystemId, out listPrice);
            }
            catch
            {
                return false;
            }
        }
    }
}