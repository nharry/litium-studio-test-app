﻿using litium_studio_test_app.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Security;
using Litium.Products;
using Litium.Products.StockStatusCalculator;
using Litium.Web.Products.Routing;

namespace litium_studio_test_app.Core.Services
{
    public class StockService : IStockService
    {
        private readonly IStockStatusCalculator _stockStatusCalculator;
        private readonly IStockStatusMessageService _stockStatusMessageService;
        private Lazy<Variant> _variant;


        public StockService(ProductPageData productPageData, VariantService variantService, IStockStatusCalculator stockStatusCalculator, IStockStatusMessageService stockStatusMessageService)
        {
            _stockStatusCalculator = stockStatusCalculator;
            _stockStatusMessageService = stockStatusMessageService;
            _variant = new Lazy<Variant>(() => productPageData?.VariantSystemId == null
                ? null
                : variantService.Get(productPageData.VariantSystemId.Value));
        }
        public string GetStockStatusDescription(string sourceId = "")
        {
            if (_variant == null || _variant.Value.InventoryItems.FirstOrDefault() == null)
            {
                return string.Empty;
            }

            var variantInventoryItem = _variant.Value.InventoryItems.FirstOrDefault();
            if (variantInventoryItem != null)
            {
                return _stockStatusMessageService.Populate(new StockStatusCalculatorArgs
                    {
                        Date = HttpContext.Current.Timestamp,
                        UserSystemId = SecurityToken.CurrentSecurityToken.UserID,
                        WebSiteSystemId = CurrentState.Current.WebSite.ID,
                        SourceId = sourceId,
                        InventorySystemId = CurrentState.Current.WebSite.InventoryId
                    },
                    new StockStatusCalculatorItemArgs
                    {
                        VariantSystemId = _variant.Value.SystemId,
                        Quantity = Decimal.One
                    },
                    new StockStatusCalculatorResult
                    {
                        InStockQuantity = variantInventoryItem.InStockQuantity
                    }).Description;
            }

            var calculatorArgs = new StockStatusCalculatorArgs()
            {
                Date = HttpContext.Current.Timestamp,
                UserSystemId = SecurityToken.CurrentSecurityToken.UserID,
                WebSiteSystemId = (CurrentState.Current.WebSite.ID),
                SourceId = sourceId,
                InventorySystemId = CurrentState.Current.WebSite.InventoryId
            };
            var calculatorItemArgs = new StockStatusCalculatorItemArgs()
            {
                VariantSystemId = _variant.Value.SystemId,
                Quantity = Decimal.One
            };

            StockStatusCalculatorResult calculatorResult;

            return !_stockStatusCalculator.GetStockStatuses(calculatorArgs, calculatorItemArgs).TryGetValue(_variant.Value.SystemId, out calculatorResult)
                ? string.Empty
                : calculatorResult.Description;
        }

        public string GetStockStatusDescription(Variant variant, string sourceId = "")
        {
            if (variant == null)
            {
                return string.Empty;
            }

            var variantInventoryItem = variant.InventoryItems.FirstOrDefault();
            if (variantInventoryItem != null)
            {
                return _stockStatusMessageService.Populate(new StockStatusCalculatorArgs
                    {
                        Date = HttpContext.Current.Timestamp,
                        UserSystemId = SecurityToken.CurrentSecurityToken.UserID,
                        WebSiteSystemId = CurrentState.Current.WebSite.ID,
                        SourceId = sourceId,
                        InventorySystemId = CurrentState.Current.WebSite.InventoryId
                    },
                    new StockStatusCalculatorItemArgs
                    {
                        VariantSystemId = variant.SystemId,
                        Quantity = Decimal.One
                    },
                    new StockStatusCalculatorResult
                    {
                        InStockQuantity = variantInventoryItem.InStockQuantity
                    }).Description;
            }

            var calculatorArgs = new StockStatusCalculatorArgs()
            {
                Date = HttpContext.Current.Timestamp,
                UserSystemId = SecurityToken.CurrentSecurityToken.UserID,
                WebSiteSystemId = (CurrentState.Current.WebSite.ID),
                SourceId = sourceId,
                InventorySystemId = CurrentState.Current.WebSite.InventoryId
            };
            var calculatorItemArgs = new StockStatusCalculatorItemArgs()
            {
                VariantSystemId = variant.SystemId,
                Quantity = Decimal.One
            };

            StockStatusCalculatorResult calculatorResult;

            return !_stockStatusCalculator.GetStockStatuses(calculatorArgs, calculatorItemArgs).TryGetValue(variant.SystemId, out calculatorResult)
                ? string.Empty
                : calculatorResult.Description;
        }
    }
}