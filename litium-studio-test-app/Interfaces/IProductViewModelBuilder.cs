﻿using Litium.Products;
using litium_studio_test_app.ViewModels;
using System;

namespace litium_studio_test_app.Interfaces
{
    public interface IProductViewModelBuilder
    {
        ProductViewModel CreateProductViewModelFromProductId(Guid productId, DateTime currentTime, Guid webSiteId);
        ProductViewModel CreateProductViewModelFromVariantId(Guid variantId, DateTime currentTime, Guid webSiteId);
        ProductViewModel CreateProductViewModelFromBaseProduct(BaseProduct baseProduct, DateTime currentTime, Guid webSiteId);
        ProductViewModel CreateProductViewModelFromVariant(BaseProduct baseProduct, Variant variant, DateTime currentTime, Guid webSiteId);
        VariantViewModel CreateVariantViewModel(Variant variant, DateTime currentTime);
    }
}