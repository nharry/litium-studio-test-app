﻿using litium_studio_test_app.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Litium.Web.Models.Products;
using Litium.Foundation.Modules.ECommerce.Plugins.PriceCalculator;

namespace litium_studio_test_app.Interfaces
{
    public interface IPriceService
    {
        IEnumerable<PriceModel> GetPriceModels(ProductModel productModel, DateTime currentTime);
        CampaignPriceResult GetCampaignPrice(ProductModel productModel, DateTime currentTime);
        bool IsPriceFound(ProductModel currentVariant, DateTime timeStamp);
    }
}
