﻿using Litium.Foundation.Modules.CMS.Content;
using Litium.Products;
using Litium.Studio.Builders.CMS;
using Litium.Web.Models.Products;
using System;
using System.Collections.Generic;

namespace litium_studio_test_app.Interfaces
{
    public interface IProductService
    {
        RelatedModel GetVariantRelationships(string variantId, string relationTypeName, bool includeBaseProductRelations = true, bool includeVariantRelations = true);

        RelatedModel GetBaseProductRelationships(BaseProduct baseProduct, string relationTypeName, bool includeBaseProductRelations = true, bool includeVariantRelations = true);

        List<Variant> FilterProductList(PageTypeDefinition currentPageTypeDefinition, PropertyCollection currentSettings, Guid productList, DateTime timeStamp, bool randomProductOrder, int top = -1);
    }
}