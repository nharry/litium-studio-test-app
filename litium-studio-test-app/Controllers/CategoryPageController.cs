﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.PageTypes;
using Litium.Foundation.Modules.ProductCatalog.Routing;
using Litium.Products;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Web.Products.Routing;
using Litium.Foundation.Modules.ExtensionMethods;
using System.Linq;
using System.Web.Services;
using litium_studio_test_app.ViewModels;
using Litium;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace litium_studio_test_app.Controllers
{
    public class CategoryPageController : LitiumController
    {

        private readonly ICategoryPageViewModelBuilder _categoryPageViewModelBuilder;
        private readonly WebSiteStrings _webSiteStrings;
        private Lazy<Category> _category;

        public CategoryPageController(Litium.Web.Products.Routing.ProductPageData productPageData, CategoryService categoryService, ICategoryPageViewModelBuilder categoryPageViewModelBuilder, WebSiteStrings webSiteString)
        {         
            _categoryPageViewModelBuilder = categoryPageViewModelBuilder;
            _webSiteStrings = webSiteString;
            _category = new Lazy<Category>(() => productPageData?.CategorySystemId == null ? null : categoryService.Get(productPageData.CategorySystemId.Value));
        }

        [HttpGet]
        public ActionResult Index(ProductPageType pageType, Page page)
        {

          

            var categoryPageModel = GetCategoryPageViewModel();

            ProductViewModel _productViewModel;
            List<ProductViewModel> lstProductViewModels = new List<ProductViewModel>();
            CategoryPageViewModel _categoryPageViewModel =new CategoryPageViewModel();

           
            var varaintsListList = categoryPageModel.ProductList.Select(x => x.Variants).ToList();
            foreach (var elemt in varaintsListList.FirstOrDefault())
            {

                var variantService = IoC.Resolve<VariantService>();
                var selectedVariant = variantService.Get(elemt.Id);

                _productViewModel = new ProductViewModel(); ;
                _productViewModel.Name = elemt.Name;
                _productViewModel.Id = elemt.Id;
                _productViewModel.Description = elemt.Description;
                _productViewModel.ProductBaseColor = elemt.VariantBaseColor;
                _productViewModel.Images = elemt.Images;
                _productViewModel.ProductPrice = selectedVariant.Prices.Select(x=>x.Price).FirstOrDefault();
                lstProductViewModels.Add(_productViewModel);

            }
            _categoryPageViewModel.ProductList = lstProductViewModels;
            return View(_categoryPageViewModel);

        }

        private CategoryPageViewModel GetCategoryPageViewModel()
        {
            var categoryPageModel = new CategoryPageViewModel();

            categoryPageModel = _categoryPageViewModelBuilder
                .CreateCategoryPageViewModel(_category.Value, CurrentState.Current.Page, CurrentState.Current.WebSite.ID);

            return categoryPageModel;
           

        }





    }

 
}