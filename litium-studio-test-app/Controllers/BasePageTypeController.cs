﻿using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS.Pages;
using litium_studio_test_app.PageTypes;
using System.Linq;
using Litium.Foundation.Modules.CMS;
using Litium.Studio.Builders;

namespace litium_studio_test_app.Controllers
{
    public class BasePageTypeController : Controller
    {
        public ActionResult BasePage(BasePageType basePageType, Page currentPage)
        {

            string imageUrl;
            var websiteStrings = currentPage.WebSite.As<WebSiteStrings>();

            if (Litium.Foundation.Modules.MediaArchive.ModuleMediaArchive.Instance.PermissionManager.UserHasFileReadPermission(CurrentState.Current.User.ID, basePageType.SiteLogo, true, true))
            {
                imageUrl =
                   Litium.Foundation.Modules.MediaArchive.Files.Image.GetFromID(
                       Litium.Foundation.Modules.MediaArchive.ModuleMediaArchive.Instance, basePageType.SiteLogo,
                       CurrentState.Current.Token)?.GetUrlToImage(100, 100, 100, 100);

            }
            else
            {
                imageUrl = "empty";
            }

            BasePageModel b = new ViewModels.BasePageModel();
            var serviceItems = basePageType.Services.Select(x =>
                                  new SelectListItem()
                                  {
                                      Text = x.ToString()
                                  });

            var model = new BasePageModel()
            {
                SiteLogoImageUrl = imageUrl,
                SiteName = basePageType.WebSiteName,
                SiteServices = basePageType.Services,
                Items = serviceItems,
                helloWorldText = websiteStrings.helloWorldText

            };

            ViewBag.vbServices = new SelectList(serviceItems, "Text", "Text", 1);

            return View(model);

        }
    }
}
    


