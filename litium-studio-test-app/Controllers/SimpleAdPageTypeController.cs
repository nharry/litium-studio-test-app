﻿using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.PageTypes;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.CMS;

namespace litium_studio_test_app.Controllers
{

    public class SimpleAdsPageTypeController : Controller
    {
        string image;
        private string redirectInternalUrl;
        private string redirectExternalUrl;
        public ActionResult AdPage(SimpleAdsPageType simpleAdsPage , Page currentPage)
        {


            
              PageDTO  _pageDto;
              var _currentPageChilds = currentPage.Children.ChildrenIDs;
              List<Litium.Foundation.Modules.CMS.Pages.Page> Childpages = Litium.Foundation.Modules.CMS.Pages.Page.GetFromIDs(_currentPageChilds, CurrentState.Current.Token);
              int count = Childpages.Count;
              foreach(var elmt in Childpages)
                {
                    _pageDto = new PageDTO();
                    _pageDto.PageId = elmt.ID;
                    _pageDto.CreatedUserId = elmt.CreatedUserID;
                    _pageDto.IsMasterPage = elmt.IsMasterPage;
                    _pageDto.Ispublished = elmt.IsPublished;
                    _pageDto.pageType = elmt.PageType;
                    _pageDto.pageShortName = elmt.ShortName;
                    _pageDto.PageStatus = elmt.Status;
                    _pageDto.pageUrl = elmt.GetUrlToPage() ;
                    _pageDto.IsSharedPage = elmt.IsSharedPage;                  

                 }
           
            if (Litium.Foundation.Modules.MediaArchive.ModuleMediaArchive.Instance.PermissionManager.UserHasFileReadPermission(CurrentState.Current.User.ID, simpleAdsPage.ImgPro, true, true))
            {
                image =
                   Litium.Foundation.Modules.MediaArchive.Files.Image.GetFromID(
                       Litium.Foundation.Modules.MediaArchive.ModuleMediaArchive.Instance, simpleAdsPage.ImgPro,
                       CurrentState.Current.Token)?.GetUrlToImage(200, 200, 200, 200);

               

            }
            else
            {
                image = "empty";
            }

            redirectInternalUrl = simpleAdsPage.NewLinkInternal?.GetUrlToPage();
            redirectExternalUrl = simpleAdsPage.NewLinkInternal?.GetUrlToPage();
            var model = new AdFormModel()
            {
                AdHeader = simpleAdsPage.PageHeader,
                AddSubHeader = simpleAdsPage.PageSubHeader,
                IsShowSubHeader = simpleAdsPage.IsSubHeaderRequired,
                ProductDescription = simpleAdsPage.ProductDescription,
                ProductColour = simpleAdsPage.ProductColour,
                Img = image,
                ProductLogoType = simpleAdsPage.ProLogoType,
                ProductDateOfLaunch = simpleAdsPage.DateOfLaunch,
                RedirectInternalPageUrl = redirectInternalUrl,
                RedirectExternalPageUrl=redirectExternalUrl

            };

            return View(model);
        }
    }

    public class PageDTO
    {

        public System.Guid PageId { get; set; }
        public System.Guid CreatedUserId { get; set; }

        public System.Type GetPageSystemType { get; set; }

        public bool IsMasterPage { get; set; }

        public bool Ispublished { get; set; }

        public Litium.Foundation.Modules.CMS.PageTypes.PageType pageType { get; set; }

        public string pageShortName { get; set; }

        public PageStatus PageStatus { get; set; }

        public string pageUrl { get; set; }

        public bool IsSharedPage { get; set; }
       
    }

}


