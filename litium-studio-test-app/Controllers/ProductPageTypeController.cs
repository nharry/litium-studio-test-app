﻿using Litium.Foundation.Modules.CMS.Pages;
using Litium.Products;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using litium_studio_test_app.Core.Services;
using Litium.FieldFramework;
using litium_studio_test_app.ViewModels;
using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Web.Models.Products;
using System.Collections.ObjectModel;

namespace litium_studio_test_app.Controllers
{
    public class ProductPageTypeController : LitiumController
    {
        private readonly  Guid _guid = Guid.NewGuid();  
        private BaseProduct _baseProduct;
        private IEnumerable<Variant> _publishedVariants;
        private readonly IProductService _productService;
        private readonly IProductViewModelBuilder _productViewModelBuilder;
        private readonly Litium.Web.Products.Routing.ProductPageData _productPageData;
        private CategoryService _categoryService;
        private readonly BaseProductService _baseProductService;
        private readonly WebSiteStrings _webSiteStrings;
        private ProductPageViewModel _productPageViewModel;



        public ProductPageTypeController(Litium.Web.Products.Routing.ProductPageData productPageData, CategoryService categoryService, BaseProductService baseProductService, IProductService productService, IProductViewModelBuilder productViewModelBuilder, WebSiteStrings webSiteStrings)
        {
           
            _productPageData = productPageData;
            _categoryService = categoryService;
            _baseProductService = baseProductService;
            _productViewModelBuilder = productViewModelBuilder;
            _productService = productService;
            _webSiteStrings = webSiteStrings;
        }
      

        //GET: ProductPageType
        public ActionResult Index(ProductPageType productPagetype, Page currentPage)
        {
            _productPageViewModel = new ProductPageViewModel();
            _productPageViewModel.ListVariantViewModel = new List<VariantViewModel>();

            if (_productPageData.BaseProductSystemId == null) throw new Exception("invalid product");
            _baseProduct = _baseProductService.Get((Guid)_productPageData.BaseProductSystemId);


            //ViewBag.firstLink = productPagetype.LinkInternal?.GetUrlToPage();

            if (!string.IsNullOrEmpty(Request.QueryString["itemid"]))
            {
                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                 var selectedVariant=  _publishedVariants.FirstOrDefault(x => x.Id == Request.QueryString["itemid"]);
                
                 var  selectedVariantViewModel = GetVariantViewModel(selectedVariant);
                _productPageViewModel.ProductViewModel = new ProductViewModel();
                _productPageViewModel.ProductViewModel.Name = selectedVariantViewModel.Name;
                _productPageViewModel.ProductViewModel.Description = selectedVariantViewModel.Description;
                _productPageViewModel.ProductViewModel.Images = selectedVariantViewModel.Images;
                _productPageViewModel.ProductViewModel.ProductBaseColor = selectedVariantViewModel.VariantBaseColor;
                _productPageViewModel.ProductViewModel.Id = selectedVariantViewModel.Id;
                _productPageViewModel.ProductViewModel.InternalMemory = selectedVariantViewModel.InternalMemory;
                _productPageViewModel.ProductViewModel.Specification = selectedVariantViewModel.Specification;
                if (selectedVariant != null)
                    _productPageViewModel.ProductViewModel.ProductPrice =
                        selectedVariant.Prices.Select(x => x.Price).FirstOrDefault();


                return View("ProductDetails", _productPageViewModel);
            }
            else if(!string.IsNullOrEmpty(Request.QueryString["method"]))
            {
                var cartService = IoC.Resolve<ICartService>();
                var cartItems = cartService.GetCart();

                var productModelBuilder = IoC.Resolve<ProductModelBuilder>();
                CartItemViewModel cartItemViewModel = new CartItemViewModel();
                ProductViewModel _productViewModel;

                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                cartItemViewModel.LstProductViewModels = new List<ProductViewModel>();

                foreach (var item in cartItems.Items)
                {
                   

                    var selectedVariant = _publishedVariants.FirstOrDefault(x => x.Id == item.ProductViewModel.Id);
                    var productModel = productModelBuilder.BuildFromVariant(selectedVariant);
                    var variantViewModel = GetVariantViewModel(productModel, DateTime.Now);

                    if (selectedVariant != null)
                    {
                        _productViewModel = new ProductViewModel();
                        _productViewModel.Id = variantViewModel.Id;
                        _productViewModel.Name = variantViewModel.Name;
                        _productViewModel.Images = variantViewModel.Images;

                        _productViewModel.ProductPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault();
                        cartItemViewModel.LstProductViewModels.Add(_productViewModel);
                    }

                   
                }


                return  View("Cart", cartItemViewModel);
            }
            else 
            {

                var product = _productPageData.GetProductForWebSite(CurrentState.Current.WebSiteID);
                _productPageViewModel = GetPageViewModel();
                _publishedVariants = _baseProduct.GetPublishedVariants(CurrentState.Current.WebSiteID);
                VariantViewModel _variantViewModel;
                _productPageViewModel.CurrentWebsiteId = CurrentState.Current.WebSiteID.ToString();

                _productPageViewModel.ListVariantViewModel = new List<VariantViewModel>();

                foreach (var variantItem in _publishedVariants)
                {
                    _variantViewModel = new VariantViewModel();
                    _variantViewModel = GetVariantViewModel(variantItem);
                    _productPageViewModel.ListVariantViewModel.Add(_variantViewModel);

                }

                return View("ProductDetails", _productPageViewModel);
            }
  
        }



        private NewVariantViewModel GetVariantViewModel(ProductModel productModel, DateTime currentTime)
        {
            ImageViewModelBuilder imageViewModelBuilder = new ImageViewModelBuilder();
            ReadOnlyCollection<ImageViewModel> images = null;
            if (productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images) != null)
            {
                var imageIds = productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images);
                images = new ReadOnlyCollection<ImageViewModel>(imageIds.Select(id => imageViewModelBuilder.CreateImageViewModel(id)).ToList());
            }

            var variantViewModel = new NewVariantViewModel()
            {
                Id = productModel.SelectedVariant.Id,
                Name = productModel.GetValue<string>("_name", CurrentState.Current.Culture),
                UseVariantUrl = productModel.UseVariantUrl,
                Images = images,
                Description = productModel.GetValue<string>("_description", CurrentState.Current.Culture),
                ProductLongText = productModel.GetValue<string>("ProductLongText", CurrentState.Current.Culture),
                SeoDescription = productModel.GetValue<string>("_seoDescription", CurrentState.Current.Culture),
                SeoTitle = productModel.GetValue<string>("_seoTitle", CurrentState.Current.Culture),
                ProductWebPreambleTitle = productModel.GetValue<string>("ProductWebPreambleTitle", CurrentState.Current.Culture),
                ProductWebPreambleText = productModel.GetValue<string>("ProductWebPreambleText", CurrentState.Current.Culture),
                VariantBaseColor = productModel.GetValue<string>("Colour", CurrentState.Current.Culture),
                InternalMemory = productModel.GetValue<string>("InternalMemory", CurrentState.Current.Culture),
                Specification = productModel.GetValue<string>("Specification", CurrentState.Current.Culture),

            };

            return variantViewModel;
        }



        //[HttpPost]
        public ActionResult ViewParticulrProduct()
        {
            return View("ProductDetails");
        }

        private ProductPageViewModel GetPageViewModel()
        {
            var productPageModel = new ProductPageViewModel();

            productPageModel.ProductViewModel = _productViewModelBuilder
                .CreateProductViewModelFromBaseProduct(_baseProduct, ControllerContext.HttpContext.Timestamp, CurrentState.Current.WebSite.ID);

            return productPageModel;
        }

        private VariantViewModel GetVariantViewModel(Variant _variant)
        {
            var variantViewModel=new VariantViewModel();
           
            variantViewModel = _productViewModelBuilder.CreateVariantViewModel(_variant, DateTime.Now);
            return variantViewModel;
        }

    


    }
}