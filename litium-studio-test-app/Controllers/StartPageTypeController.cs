﻿using Litium.Foundation.Modules.CMS.Pages;
using litium_studio_test_app.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Collections.Generic;
using System.Web.Mvc;
using litium_studio_test_app.PageTypes;
using litium_studio_test_app.ViewModels;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.CMS;

namespace litium_studio_test_app.Controllers
{
    public class StartPageTypeController : Controller
    {

        private string redirectInternalUrl;
        private string firstredirectInternalUrl;


        // GET: StartPageType
        public ActionResult StartPage(StartPageType startPageType, Page currentPage)
        {
            //redirectInternalUrl = startPageType.LinkInteralPages[0]?.GetUrlToPage();
            firstredirectInternalUrl = startPageType.LinkToInteralPages?.GetUrlToPage();
            var model = new AdFormModel()
            {
                RedirectInternalPageUrl = firstredirectInternalUrl
            };

            return View(model);
     

        }
    }
}