﻿using System.Globalization;
using System.Linq;
using Litium.FieldFramework;
using Litium.FieldFramework.FieldTypes;

namespace litium_studio_test_app.Extensions
{
    public static class FieldDefinitionExtensions
    {

        public static string GetTranslation(this IFieldDefinition field, string key, CultureInfo culture)
        {
            if (key == null)
            {
                return null;
            }
            var options = field.Option as TextOption;
            var option = options?.Items.FirstOrDefault(x => x.Value == key);
            if (option == null)
            {
                return key;
            }

            string translation;
            if (option.Name.TryGetValue(culture?.Name ?? CultureInfo.CurrentCulture.Name, out translation))
                return translation;
            return key;
        }
    }
}
