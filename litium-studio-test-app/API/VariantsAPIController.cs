﻿using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.ExtensionMethods;
using Litium.Products;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using litium_studio_test_app.Core.Services;
using Org.BouncyCastle.X509.Store;
using System.Collections.ObjectModel;
using Litium.FieldFramework;
using Litium.FieldFramework.Internal;
using Litium.Web.Models.Products;

namespace litium_studio_test_app.Controllers
{

    public class VariantsApiController : ApiController
    {

        [Route("varaint")]
        [HttpPost]
        public IHttpActionResult GetVariantDetails(NewVariantViewModel  model)
        {
            var variantService = IoC.Resolve<VariantService>();
            var productModelBuilder = IoC.Resolve<ProductModelBuilder>();

            var selectedVariant = variantService.Get(model.Id);          
            var productModel = productModelBuilder.BuildFromVariant(selectedVariant);
            var variantViewModel = GetVariantViewModel(productModel, DateTime.Now);           
            variantViewModel.VariantItemPrice = selectedVariant.Prices.Select(x => x.Price).FirstOrDefault();         
            return Ok(variantViewModel);
        }

        private NewVariantViewModel GetVariantViewModel(ProductModel productModel, DateTime currentTime)
        {
            ImageViewModelBuilder imageViewModelBuilder = new ImageViewModelBuilder();    
            ReadOnlyCollection<ImageViewModel> images = null;
            if (productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images) != null)
            {
                var imageIds = productModel.GetValue<ReadOnlyCollection<Guid>>(SystemFieldDefinitionConstants.Images);
                images = new ReadOnlyCollection<ImageViewModel>(imageIds.Select(id => imageViewModelBuilder.CreateImageViewModel(id)).ToList());
            }
       
            var variantViewModel = new NewVariantViewModel()
            {
                Id = productModel.SelectedVariant.Id,
                Name = productModel.GetValue<string>("_name", CurrentState.Current.Culture),
                UseVariantUrl = productModel.UseVariantUrl,
                Images = images,
                Description = productModel.GetValue<string>("_description", CurrentState.Current.Culture),
                ProductLongText = productModel.GetValue<string>("ProductLongText", CurrentState.Current.Culture),
                SeoDescription = productModel.GetValue<string>("_seoDescription", CurrentState.Current.Culture),
                SeoTitle = productModel.GetValue<string>("_seoTitle", CurrentState.Current.Culture),
                ProductWebPreambleTitle = productModel.GetValue<string>("ProductWebPreambleTitle", CurrentState.Current.Culture),
                ProductWebPreambleText = productModel.GetValue<string>("ProductWebPreambleText", CurrentState.Current.Culture),            
                VariantBaseColor = productModel.GetValue<string>("Colour", CurrentState.Current.Culture),
                InternalMemory = productModel.GetValue<string>("InternalMemory", CurrentState.Current.Culture),
                Specification = productModel.GetValue<string>("Specification", CurrentState.Current.Culture), 
  
            };

            return variantViewModel;
        }

    }
}
