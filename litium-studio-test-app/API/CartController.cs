﻿using Litium.Foundation.Modules.CMS;
using litium_studio_test_app.Interfaces;
using litium_studio_test_app.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace litium_studio_test_app.API
{
    public class CartController : ApiController
    {

        private  ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [Route("GetCart")]
        [HttpGet]
        public CartViewModel GetCart()
        {
            return _cartService.GetCart();
        }

        [Route("AddItem/{articleNumber}/{quantity}")]
        [HttpGet]
        public IHttpActionResult AddItem(string articleNumber =null, int quantity=0)
        {
           //CurrentState.Current.ShoppingCart.Clear();
            _cartService.AddItem(articleNumber, quantity);       
            return Ok(_cartService.GetCart());

        }

        [HttpGet]
        [Route("Clear")]
        public IHttpActionResult ClearShoppingCart()
        {
            CurrentState.Current.ShoppingCart.Clear();
            return Ok();
        }

        [Route("RemoveItem/{articleNumber}")]
        [HttpGet]
        public CartViewModel RemoveItem(string articleNumber)
        {
            _cartService.RemoveItem(articleNumber);
            return _cartService.GetCart();
        }





    }
}
