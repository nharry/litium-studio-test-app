﻿using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.WebSites;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace litium_studio_test_app
{
    [JsonObject(MemberSerialization.OptIn)]
    public class WebSiteStrings : WebSiteStringDefinition
    {
        protected override IEnumerable<WebSite> Setup()
        {
            return ModuleCMS.Instance.WebSites;
        }

        [Translation("Hej världen", "sv-SE")]
        [Translation("Hello World", "en-us")]
        public virtual string helloWorldText { get; set; }

    }
}