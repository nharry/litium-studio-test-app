﻿using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;
using litium_studio_test_app.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.REGULAR, nameof(StartPageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = false,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = false,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true,
        PossibleParentPageTypes = "*")]
    [Translation("Start Page", "sv-se")]
    [Translation("Start Page", "en-us")]

    [MvcTemplate("StartPage", "Images/Page.gif", typeof(StartPageTypeController), "StartPage", Overwrite = true)]
    public class StartPageType : PageTypeDefinitionBase
    {
        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page header", "en-us")]
        public virtual string PageHeader { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Ads Contents")]
        [Translation("Page sub header", "en-us")]
        public virtual string PageSubHeader { get; set; }

        [Property(PropertyCollectionTypes.SETTINGS, typeof(BooleanProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true)]
        [Translation("Sub header required", "en-us")]
        public virtual bool IsSubHeaderRequired { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page[] LinkInteralPages { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkToInteralPages { get; set; }


        protected override List<string> RemovePropertyNames
        {
            get { return new List<string>() { }; }
        }

    }
}