﻿using System.Collections.Generic;
using litium_studio_test_app.Controllers;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;
using System;
using Litium.Foundation.Modules.CMS.Pages;

namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.PRODUCT_CATALOG, nameof(ProductPageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = true,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = true,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true,
        PossibleParentPageTypes = "*")]
    [Translation("Product Page Type", "sv-se")]
    [Translation("Product Page Type", "en-us")]
    [MvcTemplate("ProductPageType", "Images/Page.gif", typeof(ProductPageTypeController), nameof(ProductPageTypeController.Index), Overwrite = true)]  
    public class ProductPageType : PageTypeDefinitionBase
    {


        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page [] NewLinkInternal { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(LinkInternalProperty), IsMandatory = true, Overwrite = true, ShowInGuide = true, Group = "Ads Contents")]
        public virtual Page LinkInternal { get; set; }

        protected override List<string> RemovePropertyNames
        {
            get { return new List<string>() { }; }
        }




    }
}

