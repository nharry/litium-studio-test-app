﻿using System.Collections.Generic;
using litium_studio_test_app.Controllers;
using Litium.Foundation.Modules.CMS.Content;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Studio.Builders.Attributes;
using Litium.Studio.Builders.CMS;
using System;

namespace litium_studio_test_app.PageTypes
{
    [PageType(PageTypeCategories.REGULAR, nameof(BasePageType),
        Overwrite = true,
        CanBeArchived = true,
        AutoArchiveWeeks = -1,
        CanBeMasterPage = true,
        CanBeMovedToTrashcan = true,
        CanBeInMenu = true,
        CanBeInSiteMap = true,
        CanBeLinkedTo = true,
        CanBePrinted = true,
        CanBeSearched = true,
        CanBeVersioned = true,
        VersionsToKeep = 10,
        CanBeInVisitStatistics = true,
        EditableInGui = true,
        UseSecureConnection = false,
        CanDeletePageType = true
        )]
    [Translation("Base Page", "sv-se")]
    [Translation("Base Page", "en-us")]
    [MvcTemplate("BasePage", "Images/Page.gif", typeof(BasePageTypeController), "BasePage", Overwrite = true)]
    public class BasePageType : PageTypeDefinitionBase
    {

        [Property(PropertyCollectionTypes.CONTENT, typeof(ImageFromMediaArchiveProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Logos")]
        public virtual Guid SiteLogo { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Texts")]
        [Translation("Web Site Name", "en-us")]
        [Translation("Web Site Name", "sv-se")]
        public virtual string WebSiteName { get; set; }

        [Property(PropertyCollectionTypes.CONTENT, typeof(StringShortProperty), IsMandatory = true, ShowInGuide = true, Overwrite = true, Group = "Fills")]
        public virtual IList<string> Services { get; set; }

        protected override List<string> RemovePropertyNames
        {
            get { return new List<string>() { "PageHeader", "SiteName" }; }
        }
    }
}

