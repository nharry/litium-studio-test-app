﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium;
using Litium.Foundation.Modules.CMS;
using Litium.Foundation.Modules.CMS.Pages;
using Litium.Foundation.Modules.CMS.PageTypes;
using Litium.Foundation.Modules.CMS.WebSites;
using Litium.Foundation.Security;
using Litium.Studio.Builders;
using Litium.Studio.Builders.CMS;

namespace litium_studio_test_app.PageTypes
{
    public abstract class PageTypeDefinitionBase : PageTypeDefinition
    {
        protected abstract List<string> RemovePropertyNames { get; }
        
        public static PageType GetPageType<T>() where T : PageTypeDefinitionBase
        {
            return ModuleCMS.Instance.PageTypes.GetPageType(typeof(T).Name);
        }

        public static T GetPage<T>(Guid pageId, SecurityToken token) where T : PageTypeDefinitionBase
        {
            var page = Page.GetFromID(pageId, token);
            return page.As<T>();
        }

        public static List<Page> GetInstances<T>(Guid webSiteId, SecurityToken token) where T : PageTypeDefinitionBase
        {
            try
            {
                var instanceIds = GetPageType<T>().Instances.GetPublishedPageIds(webSiteId, token);
                return Page.GetFromIDs(instanceIds, token);
            }
            catch (Exception exception)
            {
                typeof(T).Log().Error("Cannot get pagetype instances", exception);
                return new List<Page>();
            }
        }

        public static List<T> GetInstancesInstance<T>(Guid webSiteId, SecurityToken token) where T : PageTypeDefinitionBase
        {
            try
            {
                var pages = GetInstances<T>(webSiteId, token);
                return pages.Where(p => p != null)
                    .Select(p => p.As<T>())
                    .ToList();
            }
            catch (Exception exception)
            {
                typeof(T).Log().Error("Cannot get pagetype instances", exception);
                return new List<T>();
            }
        }


        public static bool IsSingleInstancePageType<T>() where T : PageTypeDefinitionBase
        {
            return typeof(T).GetCustomAttributes(typeof(SingleInstanceAttribute), false).Any();
        }

        public static Page GetSingleInstancePage<T>(WebSite webSite, SecurityToken token) where T : PageTypeDefinitionBase
        {
            var pageId = GetPageType<T>().Instances.GetPublishedPageIds(webSite.ID, token).FirstOrDefault();
            return pageId == default(Guid) ? null : webSite.GetPage(pageId, token);
        }

        public static T GetSingleInstancePageInstance<T>(WebSite webSite, SecurityToken token) where T : PageTypeDefinitionBase
        {
            var pageId = GetPageType<T>().Instances.GetPublishedPageIds(webSite.ID, token).FirstOrDefault();
            return pageId == default(Guid) ? null : webSite.GetPage(pageId, token).As<T>();
        }

        protected override void PostSetup(bool overwriteExisting)
        {
            if (RemovePropertyNames == null)
            {
                return;
            }

            var pageTypeName = GetType().Name;
            var pageType = ModuleCMS.Instance.PageTypes.GetPageType(pageTypeName);

            foreach (var removePropertyName in RemovePropertyNames)
            {
                RemoveProperty(pageType, removePropertyName);
            }
        }

        private void RemoveProperty(PageType pageType, string propertyName)
        {
            try
            {
                var property = pageType.Content.GetProperty(propertyName);

                if (property == null)
                {
                    return;
                }

                property.Delete(ModuleCMS.Instance.AdminToken);
                this.Log().Info("Property '{0}' deleted from pagetype '{1}'", propertyName, pageType);
            }
            catch (Exception exception)
            {
                this.Log().Error(string.Format("Error removing property '{0}' from pagetype", propertyName), exception);
            }
        }

        //}

        //    return filePaths.ToList();
        //    var filePaths = attributes.Select(x => x.FilePath);
        //    var attributes = typeof(T).GetCustomAttributes(typeof(TemplateAttribute), false).Cast<TemplateAttribute>();
        //{

        //public static List<string> GetTemplateFilePaths<T>()
    }
}