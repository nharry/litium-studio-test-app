﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace litium_studio_test_app.ViewModels
{
    public class BasePageModel
    {
        public string SiteName { get; set; }
        public Guid SiteLogoImage { get; set; }
        public string SiteLogoImageUrl { get; set; }
        public IList<string> SiteServices { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public string helloWorldText { get; set; }

    }
}