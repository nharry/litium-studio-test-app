﻿using Litium.Foundation.Modules.ECommerce.Plugins.PriceCalculator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class VariantViewModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public ReadOnlyCollection<ImageViewModel> Images { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StockStatusDescription { get; set; }
        public PriceViewModel Price { get; set; }
        public CampaignPriceResult CampaignPrice { get; set; }
        //public Currency Currency { get; set; }
        public bool UseVariantUrl { get; set; }
        public bool IsInStock { get; set; }
        //public BaseProduct BaseProduct { get; set; }
        public VariantColor Color { get; set; }
        public VariantSize Size { get; set; }
        public string ProductLongText { get; set; }
        public string SeoDescription { get; set; }
        public string SeoTitle { get; set; }
        public string ProductWebPreambleTitle { get; set; }
        public string ProductWebPreambleText { get; set; }
        public bool IsSelected { get; set; }

        #region Newle Added Fields

        public string VariantBaseColor { get; set; }
        public string InternalMemory { get; set; }
        public string Specification { get; set; }  

        #endregion
    }
    public class VariantSize
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class VariantColor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Hex { get; set; }
        public bool IsSelected { get; set; }
    }

    public class PriceViewModel
    {
        public PriceItem BasePrice { get; set; }
        public PriceItem DiscountPrice { get; set; }
        public PriceItem ClubPrice { get; set; }


        public class PriceItem
        {
            public decimal Price { get; set; }
            public decimal VatPercentage { get; set; }
            public decimal MinimumQuantity { get; set; }
            public decimal PriceInclVat { get; set; }
            public string PriceText { get; set; }
        }
    }
}