﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class ProductPageViewModel
    {
        public ProductViewModel ProductViewModel { get; set; }
        public List<ProductViewModel> SimilarProducts { get; set; }
        public string SimilarHeading { get; set; }

        public List<VariantViewModel> ListVariantViewModel { get; set; }
        public string CurrentWebsiteId { get; set; }


    }
}