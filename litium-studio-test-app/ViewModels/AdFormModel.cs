﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class AdFormModel
    {
        public string AdHeader { get; set; }
        public string AddSubHeader { get; set; }
        public bool IsShowSubHeader { get; set; }
        public string ProductDescription { get; set; }
        public string ProductColour { get; set; }
        public string Img { get; set; }
        public string ProductLogoType { get; set; }
        public DateTime ProductDateOfLaunch { get; set; }
        public string RedirectInternalPageUrl { get; set; }

        public string RedirectExternalPageUrl { get; set; }
    }
}