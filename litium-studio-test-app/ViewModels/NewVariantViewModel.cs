﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace litium_studio_test_app.ViewModels
{
    public class NewVariantViewModel : VariantViewModel
    {
        public decimal VariantItemPrice { get; set; }
    }
}